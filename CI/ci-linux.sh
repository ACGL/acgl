#!/bin/bash

COMPILER=$1
LANGUAGE=$2
BUILDTYPE=$3

# Exit script on any error
set -e

# Updating/initializing submodules
git submodule update --init --recursive

OPTIONS=""
MAKE_OPTIONS=""
BUILDPATH="ci-build"
BINPATH=""
STARTPATH=`pwd`

ADDITIONAL_CMAKE_OPTIONS=""
TEST_BINARY=""

#=====================================
# Compiler Settings:
#=====================================
if [ "$COMPILER" == "gcc" ]; then
  echo "Building with GCC";

  BUILDPATH="$BUILDPATH-gcc"
  OPTIONS="-DCMAKE_CXX_COMPILER=g++ -DCMAKE_C_COMPILER=gcc"

elif [ "$COMPILER" == "clang" ]; then
  echo "Building with CLANG";

  OPTIONS="$OPTIONS -DCMAKE_CXX_COMPILER=clang++ -DCMAKE_C_COMPILER=clang"
  BUILDPATH="$BUILDPATH-clang"
else
  echo "Unknown compiler: $COMPILER"
  exit 1
fi


#=====================================
# Language Settings:
#=====================================
if [ "$LANGUAGE" == "c++98" ]; then
  echo "Building with C++98 is NOT SUPPORTED (yet)!";
  exit 1
elif [ "$LANGUAGE" == "c++11" ]; then
  echo "Building with C++11";
  #OPTIONS="$OPTIONS -DCMAKE_CXX_FLAGS='-std=c++11' "
  # option is automatic for now
  BUILDPATH="$BUILDPATH-cpp11"
else
  echo "Unknown language: $LANGUAGE"
  exit 1
fi


#=====================================
# Build type Settings:
#=====================================

if [ "$BUILDTYPE" == "debug" ]; then
  echo "Building in DEBUG";
  OPTIONS="$OPTIONS -DCMAKE_BUILD_TYPE=Debug "
  BUILDPATH="$BUILDPATH-debug"
  BINPATH="bin/Debug"
elif [ "$BUILDTYPE" == "release" ]; then
  echo "Building in RELEASE";
  OPTIONS="$OPTIONS -DCMAKE_BUILD_TYPE=Release "
  BUILDPATH="$BUILDPATH-release"
  BINPATH="bin/Release"
else
  echo "Unknown build type: $BUILDTYPE"
  exit 1
fi


#=====================================
# Max Jobs
#=====================================

JOBS=`grep -c ^processor /proc/cpuinfo`
JOBS=$(($JOBS > 8 ? 8 : $JOBS))
echo "Building with $JOBS jobs"

#=====================================
# Color Settings:
#=====================================
NC='\033[0m'
OUTPUT='\033[0;32m'
WARNING='\033[0;93m'


#=====================================
#=====================================
#=====================================

echo -e "${OUTPUT}"
echo ""
echo "======================================================================"
echo "Building $BUILDPATH"
echo "  cmake: $OPTIONS"
echo "======================================================================"
echo -e "${NC}"

cd $STARTPATH

if [ ! -d $BUILDPATH ]; then
  mkdir $BUILDPATH
fi

cd $BUILDPATH

cmake -G Ninja $ADDITIONAL_CMAKE_OPTIONS $OPTIONS ../

#build it
ninja -j $JOBS -l $JOBS

if [ -n "$TEST_BINARY" ]; then

  echo -e "${OUTPUT}"
  echo ""
  echo "======================================================================"
  echo "Running Unittests"
  echo "======================================================================"
  echo -e "${NC}"

  # going to bin/BUILDTYPE
  cd $STARTPATH
  cd $BINPATH

  #execute tests
  ./$TEST_BINARY --gtest_color=yes

fi

#=====================================
# back to start
cd $STARTPATH
